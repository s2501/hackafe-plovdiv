<?php
	// filename
	$filename = basename($_SERVER['PHP_SELF'],".php");
	$directory = '/' . basename(dirname($_SERVER['PHP_SELF']));


	// Preparing the base href string as it needs the hostname for clarification
	$basehref =
		'http' . (!isset($_SERVER['HTTPS']) ? null : 's' ) . '://'
		. $_SERVER['HTTP_HOST']
		. strtr(dirname($_SERVER['SCRIPT_FILENAME']), array($_SERVER['DOCUMENT_ROOT'] => ''))
		. '/';
	/**#@-*/

	// project name
	$project_name = 'Game of life';
