/**
 * Master application
 * Includes Classes App
 */

window.App = window.App || {};

(function($, App){

    /**
     * Initialize Application
     */
    App.defaults = {
        blockSelector: '.life-box',
        columns: 4,
        rows: 4
    };

    /**
     * Initialize Application
     */
    App.init = function() {
        UI.generateGrid();

        //When the grid has been generated bind events;
        $(document).on('grid:ready', function() {
            console.log('grid ready');
            App.bindEvents();
        });

        $(document).on('grid:rebuild', function() {
            App.unbindEvents();
            App.gridItems = [];
            UI.elements.wrapper.html('');
            UI.generateGrid();
        });

        $('.show-next-level').on('click', function() {
            App.levelUp();
        });

    };

    /**
     * Stored Grid elements
     */
    App.gridItems = [];
    App.cachedItems = [];

    /**
     * Bind Grid events
     * returns App
     */
    App.bindEvents = function() {

        $(document).on('click', this.defaults.blockSelector, function() {
            var $this = $(this);
            App.changeSingleItem($this.attr('data-oid'));
        });
    };

    /**
     * Unbind Bind Grid events
     * returns App
     */
    App.unbindEvents = function() {

        $(this.defaults.blockSelector).off('click');
        return App;
    };

     /**
     * Toggles item's state
     * returns App
     */
    App.changeSingleItem = function(oid) {
        var item = App.gridItems[oid];

        if(item.alive) {
            item.alive = false;
        }else {
            item.alive = true;
        }

        item.$el.toggleClass('dead');

    };

     /**
     * Toggles item's state
     * returns App
     */
    App.toggleObjectState = function(oid) {
        var item = App.gridItems[oid];
        console.log(App.gridItems[oid]);
        if(item.alive) {
            item.willLive = false;
        }else {
            item.willLive = true;
        }

        App.cachedItems.push(item);

        // item.$el.toggleClass('dead');
    };

    App.applyObjectState = function() {
        var cachedElementsCount = App.cachedItems.length;
        var i = 0;

            console.log(['change 0']);
        if(cachedElementsCount === 0) {
            return false;
        }

        while(i < cachedElementsCount) {
            console.log(['change', App.cachedItems[i]]);
            App.cachedItems[i].$el.toggleClass('dead');
            App.cachedItems[i].alive = App.cachedItems[i].willLive;
            i++;
        }

        App.cachedItems = [];
    };

    /**
     * Goes to next stage
     * returns App
     */
    App.levelUp = function() {
        var gridElementsCount = this.gridItems.length;
        var i = 0;
        console.log('level up');
        console.log(gridElementsCount);

        while(i < gridElementsCount) {
            // console.log(['i', i]);
            var liveNeighbours = [];

            if(i - 5 > 0) {
                if(App.gridItems[i - 5].alive) {
                    liveNeighbours.push(App.gridItems[i - 5]);
                }
            }

            if(i - 4 > -1) {
                if(App.gridItems[i - 4].alive) {
                    liveNeighbours.push(App.gridItems[i - 4]);
                }
            }

            if(i - 3 > -1) {
                if(App.gridItems[i - 3].alive) {
                    liveNeighbours.push(App.gridItems[i - 3]);
                }
            }

            if(i - 1 > -1) {
                if(App.gridItems[i - 1].alive) {
                    // console.log(['true', i, i - 1 > -1]);
                    liveNeighbours.push(App.gridItems[i - 1]);
                }
            }

            if(i + 1 < gridElementsCount) {
                if(App.gridItems[i + 1].alive) {
                    liveNeighbours.push(App.gridItems[i + 1]);
                }
            }

            if(i + 3 < gridElementsCount) {
                if(App.gridItems[i + 3].alive) {
                    liveNeighbours.push(App.gridItems[i + 3]);
                }
            }

            if(i + 4 < gridElementsCount) {
                if(App.gridItems[i + 4].alive) {
                    liveNeighbours.push(App.gridItems[i + 4]);
                }
            }

            if(i + 5 < gridElementsCount) {
                if(App.gridItems[i + 5].alive) {
                    liveNeighbours.push(App.gridItems[i + 5]);
                }
            }

            console.log(liveNeighbours);

            if(liveNeighbours.length > 2 && !App.gridItems[i].alive) {
                //resurect object
                console.log('resurect');
                App.toggleObjectState(i);
            }else if(liveNeighbours.length >= 4 && App.gridItems[i].alive){
                console.log('die overcrowded');
                //kill object
                App.toggleObjectState(i);
            }else if(liveNeighbours.length < 2 && App.gridItems[i].alive) {
                console.log('die lonely');
                //kill item
                App.toggleObjectState(i);
            }

            i++;
        }

        App.applyObjectState();
    };



    /**
     * Document ready
     */
    $(function() {
        App.init();
    });

})($, App);


var GridObject = function(options) {
    this.oid = options.oid;
    this.alive = false;
    this.willLive = false;
    this.$el = options.$el;
};
