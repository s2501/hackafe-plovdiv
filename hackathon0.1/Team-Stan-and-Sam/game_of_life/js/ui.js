/**
 * Master application
 * Includes Classes App
 */

window.UI = window.UI || {};

(function($, UI){

UI.elements = {
    $wrapper: $('.box-life-wrapper')
};

UI.generateGrid = function() {

	var columnWidth = App.defaults.columns * 21;
	this.elements.$wrapper.css('width', columnWidth);

	var numberOFelements = App.defaults.columns * App.defaults.rows;

	for (var i = 0; i < numberOFelements; i++) {

		var $elem = $('<span class="life-box dead" data-oid="' + i + '"/>');

		App.gridItems.push(new GridObject({oid: i, $el: $elem}));

		$('.box-life-wrapper').append($elem);
	}

    setTimeout(function() {
        $(document).trigger('grid:ready');
    }, 0);

};

})($, UI);