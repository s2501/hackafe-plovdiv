<?php include 'inc/env.php'; ?>
<!doctype html>

<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js eq-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <base href="<?php echo $basehref; ?>">
    <title>Game of life Prototype</title>
    <meta name="description" content="">

    <meta name="viewport" content="width=device-width">

    <!-- <link rel="stylesheet" href="css/styles.css"> -->
    <script src="js/libs/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <link rel="stylesheet/less" type="text/css" href="less/bootstrap.less">
    <script src="js/libs/less-1.3.3.min.js"></script>

</head>
<body>

    <div class="wrapper" role="main">
        <header>
            <h1>Game of life</h1>
        </header>